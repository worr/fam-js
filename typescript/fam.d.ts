interface event {
	filename: string;
	event_type: number;
}

interface NodeFAM {
	watch(callback: (event) => void): void;
	addPaths(...paths: string[]): void;
	done(): void;
	paths: string[];
}

declare var FAM: {
	new (): NodeFAM;
}
