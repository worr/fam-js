var fam = require("../fam");

var fs = require("fs");
var nodeunit = require("nodeunit");

var testfile = "/tmp/fam.js-test-add";

module.exports.test_fam_add = function(test) {
	var mon = new fam.FAM();

	mon.addPaths('/tmp');
	test.deepEqual(mon.paths, [ '/tmp' ], "single directory");

	mon.addPaths('/var/tmp');
	test.deepEqual(mon.paths, [ '/tmp', '/var/tmp' ], "added another single directory");

	mon.addPaths('/var', '/var/cache');
	test.deepEqual(mon.paths, [ '/tmp', '/var/tmp', '/var', '/var/cache' ], "added multiple directories");

	mon = new fam.FAM();
	test.throws(function() {
		mon.addPaths('/notavaliddirectory');
	}, Error, "adding nonexistant directory");

	test.throws(function() {
		mon.addPaths('/dev/stderr');
	}, Error, "adding non-regular file");

	mon = new fam.FAM();
	fs.open(testfile, 'w', function(err, fd) {
		if (! err) {
			fs.write(fd, "test", 0,  5, 0, function(err, written, buffer) {
				if (! err) {
					mon.addPaths(testfile);
					test.deepEqual(mon.paths, [ testfile ], "adding single file");
					fs.unlinkSync(testfile);
				} else {
					test.fail("Could not write test file");
				}
			});
		} else {
			test.fail("Could not write test file");
		}

		test.done();
	});
}
