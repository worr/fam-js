var fam = require("../fam");

var fs = require("fs");
var nodeunit = require("nodeunit");

var testfile = "/tmp/fam.js-test-watch";

module.exports.test_fam_watch = function(test) {
	var mon = new fam.FAM();

	mon.addPaths('/tmp');
	mon.watch(function watcher(ev) {
		test.equal(ev.filename, "fam.js-test-watch");
		console.log(ev.event_type);

		fs.unlinkSync(testfile);

		test.done();
		mon.done();
	});

	fs.open(testfile, 'w', function(err, fd) {
		if (! err) {
			fs.write(fd, "test", 0,  5, 0, function(err, written, buffer) {
				if (err) {
					test.fail("Could not write test file");
				}
			});
		} else {
			test.fail("Could not write test file");
		}
	});
}
