#include "fam.h"

#include <cstdlib>
#include <node/node.h>
#include <v8.h>

extern "C" {
	#include <fam.h>
	#include <sys/stat.h>
	#include <unistd.h>
}

using namespace v8;
using namespace node;

FAM::FAM() : count(0) {
	if ((fc = (FAMConnection*)malloc(sizeof(FAMConnection))) == NULL)
		ThrowException(Exception::Error(String::New("Cannot allocate memory for FAMConnection")));

	if ((fr = (FAMRequest*)malloc(sizeof(FAMRequest))) == NULL)
		ThrowException(Exception::Error(String::New("Cannot allocate memory for FAMRequest")));

	if (FAMOpen(fc) != 0) {
		free(fc);

		ThrowException(Exception::Error(String::New("Cannot open new FAM fd")));
	}

#ifdef __GAMIN_FAM_H__
	/*
	 * We don't want to spam people with tons of exists and endexists events
	 * when we scan a new directory if we have gamin installed.
	 */
	if (FAMNoExists(fc) != 0) {
		free(fc);

		ThrowException(Exception::Error(String::New("Cannot ignore exists/endexists events")));
	}
#endif
}

FAMConnection *FAM::getFamConnection() const {
	return fc;
}

FAMRequest *FAM::getFamRequest() const {
	return fr;
}

size_t FAM::getCount() const {
	return count;
}

void FAM::setCount(size_t count) {
	this->count = count;
}

void FAM::Initialize(Handle<Object> target) {
	HandleScope scope;

	Local<FunctionTemplate> tmpl = FunctionTemplate::New(New);
	tmpl->SetClassName(String::NewSymbol("FAM"));
	tmpl->InstanceTemplate()->SetInternalFieldCount(1);
	tmpl->PrototypeTemplate()->Set(String::NewSymbol("paths"), Array::New());
	tmpl->PrototypeTemplate()->Set(String::NewSymbol("addPaths"), 
		FunctionTemplate::New(AddPaths)->GetFunction());
	tmpl->PrototypeTemplate()->Set(String::NewSymbol("watch"),
		FunctionTemplate::New(Watch)->GetFunction());
	tmpl->PrototypeTemplate()->Set(String::NewSymbol("done"),
		FunctionTemplate::New(Done)->GetFunction());

	Persistent<Function> constructor = Persistent<Function>::New(tmpl->GetFunction());
	target->Set(String::NewSymbol("FAM"), constructor);
}

Handle<Value> FAM::New(const Arguments &args) {
	HandleScope scope;

	FAM* fm = new FAM();
	fm->Wrap(args.This());
	return args.This();
}

/*
 *  Now have to explicitly stop watching...can be called at any time
 */
Handle<Value> FAM::Done(const Arguments &args) {
	HandleScope scope;

	FAM *fm = ObjectWrap::Unwrap<FAM>(args.This());

	if (FAMCancelMonitor(fm->getFamConnection(), fm->getFamRequest())) {
		ThrowException(Exception::Error(String::New("Cannot cancel monitor session")));

		return scope.Close(Undefined());
	}

	FAMClose(fm->getFamConnection());
	free(fm->getFamConnection());
	free(fm->getFamRequest());

	return scope.Close(Undefined());
}

Handle<Value> FAM::AddPaths(const Arguments &args) {
	HandleScope scope;

	char err[FILENAME_MAX], filename[FILENAME_MAX];
	int st_ret;
	struct stat st_buf;

	FAM *fm = ObjectWrap::Unwrap<FAM>(args.This());
	FAMConnection *fc = fm->getFamConnection();
	FAMRequest *fr = fm->getFamRequest();

	for (int i = 0; i < args.Length(); i++) {
		if (!args[i]->IsString()) {
			ThrowException(Exception::TypeError(String::New("All paths must be strings")));
			return scope.Close(Undefined());
		}

		String::Cast(*args[i])->WriteUtf8(filename);
		/* Check if file exists and is readable */
		if (access(filename, R_OK) != 0) {
			snprintf(err, FILENAME_MAX, "%s is not a readable file or directory", filename);

			ThrowException(Exception::Error(String::New(err)));
			return scope.Close(Undefined());
		}

		/* stat() to figure out if it's a file or directory */
		if ((st_ret = stat(filename, &st_buf)) != 0) {
			snprintf(err, FILENAME_MAX, "Cannot stat %s", filename);

			ThrowException(Exception::Error(String::New(err)));
			return scope.Close(Undefined());
		}

		if (S_ISREG(st_buf.st_mode)) {
			if (FAMMonitorFile(fc, filename, fr, NULL) != 0) {
				snprintf(err, FILENAME_MAX, "Could not start monitoring %s", filename);

				ThrowException(Exception::Error(String::New(err)));
				return scope.Close(Undefined());
			}
		} else if (S_ISDIR (st_buf.st_mode)) {
			if (FAMMonitorDirectory(fc, filename, fr, NULL) != 0) {
				snprintf(err, FILENAME_MAX, "Could not start monitoring %s", filename);

				ThrowException(Exception::Error(String::New(err)));
				return scope.Close(Undefined());
			}
		} else {
			snprintf(err, FILENAME_MAX, "%s is not a normal file or directory", filename);

			ThrowException(Exception::Error(String::New(err)));
			return scope.Close(Undefined());
		}

		/* Add it to the path and increase path count */
		Local<Array> arr = Array::Cast(*(args.This()->Get(String::NewSymbol("paths"))));
		arr->Set(Integer::New(fm->getCount()), args[i]);
		fm->setCount(fm->getCount() + 1);
	}

	return scope.Close(Undefined());
}

Handle<Value> FAM::Watch(const Arguments &args) {
	HandleScope scope;

	if (args.Length() == 0 || !args[0]->IsFunction()) {
		ThrowException(Exception::TypeError(String::New("Argument 0 must be a function")));
		return scope.Close(Undefined());
	}

	Local<Function> cb = Local<Function>::Cast(args[0]);
	FAM *fm = ObjectWrap::Unwrap<FAM>(args.This());

	/* Create a new event and store it in our vals struct */
	fam_vals_t *vals = new fam_vals_t();
	uv_work_t *req = new uv_work_t();
	vals->fm = fm;
	if ((vals->ev = (FAMEvent*)malloc(sizeof(FAMEvent))) == NULL) {
		ThrowException(Exception::Error(String::New("Could not allocate memory for fam event")));
		return scope.Close(Undefined());
	}

	vals->cb = Persistent<Function>::New(cb);
	req->data = vals;

	fm->Ref();

	uv_queue_work(uv_default_loop(), req, UV_Watch, UV_WatchAfter);

	return scope.Close(Undefined());
}

/* Get the next event, and pass it to WatchAfter */
void FAM::UV_Watch(uv_work_t *req) {
	fam_vals_t *vals = static_cast<fam_vals_t *>(req->data);
	FAMNextEvent(vals->fm->getFamConnection(), vals->ev);
}

void FAM::UV_WatchAfter(uv_work_t *req) {
	HandleScope scope;
	fam_vals_t *vals = static_cast<fam_vals_t *>(req->data);
	vals->fm->Unref();

	/* Build our event js object */
	Local<Value> argv[1];
	Local<Object> obj = Object::New();
	obj->Set(String::NewSymbol("event_type"), Integer::New((int)vals->ev->code));
	obj->Set(String::NewSymbol("filename"), String::New(const_cast<const char*>(vals->ev->filename)));
	argv[0] = obj;

	/* Call the callback */
	TryCatch try_catch;
	vals->cb->Call(Context::GetCurrent()->Global(), 1, argv);
	if (try_catch.HasCaught()) {
		FatalException(try_catch);
	}

	vals->cb.Dispose();
	free(vals->ev);
	delete vals;
	delete req;
}

void init(Handle<Object> target) { 
	HandleScope scope;

	FAM::Initialize(target); 
}

NODE_MODULE(fam, init);
