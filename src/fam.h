#ifndef __FAM_JS_H
#define __FAM_JS_H

extern "C" {
	#include <fam.h>
}

#include <v8.h>
#include <node/node.h>

using namespace v8;
using namespace node;

class FAM : public ObjectWrap {
	private:
		FAMConnection *fc;
		FAMRequest *fr;
		char **paths;
		size_t count;

		FAM();

	public: 
		static void Initialize(Handle<Object> target);
		static Handle<Value> New(const Arguments &args);
		static Handle<Value> AddPaths(const Arguments &args);
		static Handle<Value> Watch(const Arguments &args);
		static Handle<Value> Done(const Arguments &args);
		static void UV_Watch(uv_work_t *req);
		static void UV_WatchAfter(uv_work_t *req);

		FAMConnection *getFamConnection() const;
		FAMRequest *getFamRequest() const;
		size_t getCount() const;
		void setCount(size_t count);
};

typedef struct {
	FAM* fm;
	FAMEvent* ev;
	Persistent<Function> cb;
} fam_vals_t;

#endif
