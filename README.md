fam.js
======

Fam wrapper for node

# USAGE

Grab a fam object:

    var mon = new fam.FAM()

Add some files or directories:

    mon.addPaths("/tmp");
    mon.addPaths("/tmp/foo", "/tmp/bar", "/var/tmp");

And watch!

    mon.watch(function watcher(ev) {
        console.log(ev.filename);
        console.log(ev.event_type);

        mon.watch(watcher);
    });

When you're done, close down the monitoring session!

    mon.close();

# FAQ

## Doesn't node already have something like this that doesn't rely on fam?

Yes. It's fs.watch(). But it's bad! Here's why!

1. It's listed as unstable. fam has been around forever, so it's way more stable than any bullshit the node devs put together
1. It uses inotify on linux. This is great if you _HAVE_ inotify. It sure does suck if you don't. fam works in either instance
1. It only works on some BSDs, Linux, Solaris and Windows. If you have something else, you're fucked. Good thing fam is pretty universal


## Isn't fs.watch() faster because it uses the OS's native file watching implementation?

Well...yes. However, if you want that support, install gamin instead of fam. gamin is a drop in replacement for fam, and will make use of the OS's native file wawtching syscalls if they're there.

It also handles NFS way more intelligently than fs.watch() ever could.

## Well fuck, you sure did sell me on this fam business. What do I need to use your module?

* node-waf and a c++ compiler
* mocha if you want to run the tests
* fam or gamin

## You sure do mention gamin a lot...can you tell me more?

gamin is a drop in replacement for fam made by some guy at [GNOME](http://people.gnome.org/~veillard/gamin/index.html). It uses the OS's file watching syscalls (inotify, dnotify, kqueue, etc.) if they're available, and if not, falls back to polling.

fam just straight up polls. That's dumb.

fam also likes to spam you with a ton of exists and endexists events when you watch a directory. gamin has an option to turn that off (which I've enabled when you build my module against gamin).

By default, gamin uses polling on NFS mounts. If it used inotify/dnotify/kqueue/whatever, you wouldn't get any notifications if anything changed on your NFS mounts. That sucks.

gamin is configurable. In the rare case that you want to define how gamin checks a directory...you can do that. Edit /etc/gamin/gaminrc or ~/.gaminrc to set this up. [Here's](http://people.gnome.org/~veillard/gamin/config.html) a guide!

Go modern and get gamin.
